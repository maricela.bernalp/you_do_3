#!/bin/bash
C=$1
T=$2

if [ $T = 'F' ]
then
	F=$(echo "($C * 1.8000) + 32" | bc -l )
	echo 'Resultado de conversion de centrigrados a Fahrenheit: '$F
elif [ $T = 'K' ]
then
        K=$(echo "$C + 273.15" | bc -l )
        echo 'Resultado de conversión de centrigrados a Kelvin: '$K
else 
	echo 'Verifique el parametro ingresado debe de ser [# F] [# K]'
fi

